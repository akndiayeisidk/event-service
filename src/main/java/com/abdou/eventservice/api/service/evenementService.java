package com.abdou.eventservice.api.service;

import com.abdou.eventservice.api.dto.CategorieDto;
import com.abdou.eventservice.api.entity.Evenement;
import com.abdou.eventservice.api.repository.EvenementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class evenementService {
    @Autowired
    private EvenementRepository evenementRepository;
    @Autowired
    private RestTemplate restTemplate;
    public Evenement saveEvenement(Evenement evenement){
        String responseTest = "";
         CategorieDto categorieDto = restTemplate.getForObject("http://CATEGORY-SERVICE/categorie/getOneCategorie/{id}", CategorieDto.class,evenement.getLibelleCategorie());
        evenement.setLibelleCategorie(categorieDto.getLibelle());
        System.out.println("RESPOOOOOOOONSE : "+categorieDto);
        return evenementRepository.save(evenement);
    }

}
